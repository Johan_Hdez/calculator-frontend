import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ResportsService } from 'src/app/services/resports.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  reports: any[] = [];
  reportForm: FormGroup;
  constructor(private report: ResportsService, private formBuilder: FormBuilder, private datepipe: DatePipe ) { }

  ngOnInit() {
    this.reportForm = this.formBuilder.group({
      service_identification: [''],
      technician_identification: [''],
      initial_date: [''],
      final_date: ['']
    });
  }

  search(technician: any, searchWeek: any){
    this.report.getReports(technician, searchWeek).subscribe( (data: any) => {
      this.reports = data.response.registros;
    }) 
  }

  onSubmit(){
    this.reportForm.controls.initial_date.setValue(this.datepipe.transform(this.reportForm.controls['initial_date'].value, 'yyyy-MM-dd HH:mm:ss'));
    this.reportForm.controls.final_date.setValue(this.datepipe.transform(this.reportForm.controls['final_date'].value, 'yyyy-MM-dd HH:mm:ss'));
    this.report.postReports(this.reportForm.value)
      .subscribe( data => {
        alert(JSON.parse(JSON.stringify(data)).response);
        this.reportForm.reset();

        console.log(data);
        // this.reports = data;
      })
  }

}
