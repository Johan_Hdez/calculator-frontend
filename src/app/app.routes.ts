import { Routes } from '@angular/router';
import { ReportsComponent } from './components/reports/reports.component';

export const ROUTES: Routes = [
    { path: 'reports', component: ReportsComponent },
    { path: '', pathMatch: 'full', redirectTo: 'reports' },
    { path: '**', pathMatch: 'full', redirectTo: 'reports' },
]