import { TestBed } from '@angular/core/testing';

import { ResportsService } from './resports.service';

describe('ResportsService', () => {
  let service: ResportsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResportsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
