import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ReportModel } from '../models/reports/reports.model';

@Injectable({
  providedIn: 'root'
})
export class ResportsService {

  constructor(private http: HttpClient) { }

  // private url = 'https://047ab820d0fa.ngrok.io/reports';
  private url = 'http://localhost:8085/reports';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  getQuery( query: string) {
    return this.http.get(this.url + `${ query }`);
  }

  postReports( report: ReportModel ) {
    // return this.http.post(`${ this.url }`+ '/save_report', report, this.httpOptions);
    return this.http.post(`${ this.url }` + '/save_report' , report).pipe(
      map( resp => resp)
    );

  }

  getReports( technician: any, searchWeek: any) {
    return this.http.get(this.url + `/query?id=${ technician }` + `&week=${ searchWeek }`).pipe(
      map( response => response ));
    
  }

}
