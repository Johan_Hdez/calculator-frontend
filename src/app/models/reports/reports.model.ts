
export class ReportModel {

  service_identification: string;
  technician_identification: string;
  initial_date: Date;
  final_date: Date;
  
}
